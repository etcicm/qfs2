#-------------------------------------------------------------------------------
# Name:        QFS_build
# Purpose:
#
# Author:      Alexandros
#
# Created:     16/10/2014
# Copyright:   (c) Alexandros Zachos 2014
# Licence:     GPL
#-------------------------------------------------------------------------------
import xlrd,xlwt,sys
import os
import subprocess
from datetime import datetime, timedelta
import fileinput,re
from qfs_settings import *

def main(argv):
    input = argv[1]

    countries=['AL','AT','BA','BE','BG','CH','CY','CZ','DE','DK','EE','ES','FI','FR','GB','GR','HR','HU','IE','IS','IT','LI','LT','LU','LV','ME','MK','MT','NL','NO','PL','PT','RO','RS','SE','SI','SK','TR','XK']
    country_name = xlrd.open_workbook("codelist_CountryCode.xls")
    country_names = country_name.sheet_by_index(0)

    workbook = xlrd.open_workbook(input)
    categories = workbook.sheet_names()
    country_counter = 1
    for country in countries:
        for cat in categories:
            sheet = workbook.sheet_by_name(cat)
            num_rows= sheet.nrows-1
            cur_row=1
            #tables
            while (cur_row<=num_rows):
                table_path = sheet.cell_value(cur_row,1)
                table_capture = sheet.cell_value(cur_row,3)
                path= "source/%s/tables/%s.xls" %(cat,table_path.replace('ZZ',country))
                cur_row += 1
                print path
                if os.path.isfile(path):
                    print "ok"
                    country_xls = xlrd.open_workbook(path)
                    if (cat == 'S09-Water-Quantity') or (cat == 'S07-GW-nutrients') or (cat == 'S08-GW-hazSubs'):
                        country_sheet = country_xls.sheet_by_index(0)
                    else:
                        country_sheet = country_xls.sheet_by_index(3)
                    num_cols = country_sheet.ncols-1
                    num_country_rows = country_sheet.nrows-1

                    if (num_cols>9):
                        output = open("source/%s/tables/%s.txt" %(cat,table_path),"w")
                        output.write("%s\n"%table_capture)
                        output.write("\n%s/%s\n"%(tables_root_url.replace('CC',country.lower()),table_path.replace('ZZ',country).lower()))
                        output.close()
                    elif (num_country_rows > 40):
                        output = open("source/%s/tables/%s.txt" %(cat,table_path),"w")
                        output.write("%s\n"%table_capture)
                        output.write("\n%s/%s\n"%(tables_root_url.replace('CC',country.lower()),table_path.replace('ZZ',country).lower()))
                        output.close()
                    else:
                        file = open("source/temp_files/%s.txt" %table_path,"r")
                        output = open("source/%s/tables/%s.txt"%(cat,table_path),"w")
                        text = file.read()
                        output.write(text.replace('ZZ',country))
                        output.close()
                else:
                    output = open("source/%s/tables/%s.txt"%(cat,table_path),"w")
                    print unicode(table_capture)
                    output.write("%s\n"%table_capture)
                    output.write("\nNO DATA AVAILABLE FOR THIS TABLE")
                    output.close()
            #comments
            country_comment = sheet.cell_value(1,0)
            output = open("source/%s/comments/ZZ.txt"%cat,"w")
            try:
                comment_country = open("source/%s/comments/%s.txt"%(cat,country_comment.replace('ZZ',country)),"r")
                output.write(comment_country.read())
            except (OSError, IOError) as e:
                output.write("NO COMMENTS AVAILABLE")
            output.close()
            #questions
            country_question = sheet.cell_value(1,2)
            output = open("source/%s/questions/ZZ.txt"%cat,"w")
            try:
                question_country = open("source/%s/questions/%s.txt"%(cat,country_question.replace('ZZ',country)),"r")
                output.write(question_country.read())
            except (OSError, IOError) as e:
                output.write("NO QUESTIONS AVAILABLE")
            output.close()
        #make QFS for country
        conf = open("source/conf.py").read()
        conf = conf.replace("Test-CC",country_names.cell_value(country_counter,1))
        conf_new = open("source/conf.py","w")
        conf_new.write(conf)
        conf_new.close()
        os.system('make latexpdf')
        date = datetime.now()
        os.system('cp build/latex/QFS.pdf output/QFS_%s_Part-2.pdf'%country)
        os.system('cp myBuild.log myBuild_%s.log'%country)
        os.system('make clean')
        conf = open("source/conf.py").read()
        conf = conf.replace(country_names.cell_value(country_counter,1),"Test-CC")
        conf_new = open("source/conf.py","w")
        conf_new.write(conf)
        conf_new.close()
        for cat in categories:
            os.system("rm source/%s/tables/*ZZ*"%cat)
            os.system("rm source/%s/questions/*ZZ*"%cat)
            os.system("rm source/%s/comments/*ZZ*"%cat)
        country_counter += 1

        #exit()
            #subprocess.call('C:\cygwin64\home\Alexandros\qfs-source\docs\make latexpdf')
            #subprocess.call('cp build/latex/QFS.pdf output/QFS_%s.pdf') %country


##    sheet =  workbook.sheet_by_index(3)
##    num_rows= sheet.nrows-1
##    num_cols = sheet.ncols-1
##    print num_rows
##    print num_cols
##    if (num_cols>6):
##        print"e"



if __name__ == '__main__':
    startcycle = datetime.now()
    main(sys.argv)
    print 'Done! Elapsed time:%s'%(datetime.now()-startcycle)
