
**Clarifying questions on data in current database**

#. From the dataset provided the country seem to have changed the monitoring programme from 2009 with a significant increase of the number of parameters Is this correct interpretation? What is the reason for this change?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?   

#. Almost no data have been reported on HS supporting parameters. Are such data available (especially pH, hardness, DOC) corresponding the individual samples for heavy metals?

#. As part of the QA process, it is the intention to install new rules for rejecting/flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?

#. Could you check your national databases for potential outliers (see `LINK3 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_ tables 4.4.1.1;) and, if there are outliers, correct them?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more organic substances monitored in lake water in the Netherlands and can they be delivered? (40 in organic substances sub group priority substances, 11 in sub-group additional with EQS, 15 in sub-group commonly monitored and 9 in sub-group additional where 78, 11, 21 and 40 respectively in total have been reported by the sum of countries)?

#. Can data for missing years since 2007, especially in lakes ZOOMMEER/EENDRACHT, KANAAL TERNEUZEN GENT, VEERSEMEER, VOLKERAK be redelivered? 

#. Can more years with measuring "preferred" hazardous substances be reported to increase substances and temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
