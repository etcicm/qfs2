
**Clarifying questions on data in current database**

#. No data have been reported on organic substances sub-group additional with EQS (4-OrgSub-AddEQS) in year2012. Are there more stations where substances from this sub-group are measured in these years?  Or is the stop in reporting due to very low concentrations or other reasons?

#. From the dataset provided the country seem to have changed the monitoring programme with drastic reduction of the number of parameters (up to 8 parameters in 2011, 4 parameters in 2012). What is the reason for this change?

#. In RBDs BEMaas_VL the reporting stopped (or nearly stopped) in 2011. What is the reason for this?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?   

#. For some supporting parameters (K, Mg, Na, Ca) Belgium seem to have changed the monitoring programme since 2009. What is the reason for this?  

#. As part of the QA process, it is the intention to install new rules for rejecting/flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?

#. Could you check your national databases for potential outliers (see `LINK3 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_ tables 4.4.1.1;) and, if there are outliers, correct them?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more organic substances monitored in lake water in Belgium and can they be delivered? (28 in organic substances sub group priority substances, 7 in sub-group commonly monitored, 8 in sub-group additional where 78, 21 and 40 respectively are possible) ?

#. LOD values have not been reported since 2012. What is the reason for this ?

#. Can data for missing years since 2011, esp. in RBD BEMaas_VL, be redelivered? 

#. Can more years with measuring "preferred" hazardous substances be reported to increase temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
