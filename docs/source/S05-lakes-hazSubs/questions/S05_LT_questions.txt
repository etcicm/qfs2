
**Clarifying questions on data in current database**

#. No data have been reported on all sub-groups in years 2005, 2006, 2007, 2009, 2010, 2011 and 2012. Are there more stations where substances from all sub-groups are measured in these years?  Or is the stop in reporting due to very low concentrations or other reasons?

#. Two organic substances sub groups: commonly monitored and additional (5-OrgSub-ComMon and 6-OrgSub-Add) have not been reported. Are there stations with monitoring results available ? If positive, is there a reason for not reporting via the SoE data flow ?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?   

#. No data have been reported on HS supporting parameters, except in 2008 for few parameters (Ca, SPM Temperature and pH). Are such data available (especially pH, hardness, DOC) corresponding the individual samples for heavy metals?

#. As part of the QA process, it is the intention to install new rules for rejecting/flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?

#. Could you check your national databases for potential outliers (see `LINK3 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_ tables 4.4.1.1;) and, if there are outliers, correct them?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more heavy metals and organic substances monitored in lake water in Lithuania and can they be delivered? (4 in sub group heavy metal priority substances and 4 in sub-group heavy metals additional where 8 for each are possible, 3 in organic substances sub group priority substances, 6 in sub-group additional with EQS, none in sub-group commonly monitored and none in sub-group additional where 78, 11, 21 and 40 respectively in total have been reported by the sum of countries)?

#. Can data for missing years since 2008 and between 2005 and 2007, be redelivered? 

#. Can more years with measuring "preferred" hazardous substances be reported to increase temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
