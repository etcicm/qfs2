
**Clarifying questions on data in current database**

#. No data have been reported on heavy metals priority substances sub group (1-HeaMet-PS) in years 2007 and 2008. Are there more stations where these substances are measured in these years?  Or is the stop in reporting due to very low concentrations or other reasons?

#. No data have been reported on organic substances sub-group additional with EQS, sub-group commonly monitored and sub-group additional (4-OrgSub-AddEQS, 5-OrgSub-ComMon and 6-OrgSub-Add). Are there stations with monitoring results available ? If positive, is there a reason for not reporting via the SoE data flow ?

#. No data have been reported on organic substances sub-group priority substances (3-OrgSub-PS) in years 2004, 2005, 2006, 2007, 2009, 2010. Are there more stations where 3-OrgSub-PS group is measured in these years?  Or is the stop in reporting due to very low concentrations or other reasons?

#. From the dataset provided the country seem to have changed the monitoring programme with drastic reduction of the number of stations (up to 29 stations in 2004, 3 stations in 2012). What is the reason for this change?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?   

#. As part of the QA process, it is the intention to install new rules for rejecting/flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?

#. Could you check your national databases for potential outliers (see `LINK3 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_ tables 4.4.1.1;) and, if there are outliers, correct them?

**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more heavy metals and organic substances monitored in lake water in Bosnia and Herzegovina and can they be delivered?  (4 in sub group heavy metal priority substances and 4 in sub-group heavy metals additional where 8 for each are possible, 8 in organic substances sub group priority substances and none in sub-group additional with EQS, in sub-group commonly monitored and in sub-group additional where 78, 11, 21 and 40 respectively in total have been reported by the sum of countries)?

#. LOQ values have been reported since 2003. Is it considered realistic to supply with LOQ values for older data - in particular for substances subject to review for list of priority substances? LOQ values have not been reported in 2010 and 2011, what is the reason ?

#. Can more years with measuring "preferred" hazardous substances be reported to increase temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
