
**Clarifying questions on data in current database**

#. Not much data have been reported on the three sub groups organic substances priority substances, additional with EQS and commonly monitored(3-OrgSub-PS, 4-OrgSub-AddEQS and 5-OrgSub-ComMon) in years 2006 and 2009 (only one station in 2009). Are there more stations where these sub-groups are measured in these years?  Or is the stop in reporting due to very low concentrations or other reasons?

#. No data have been reported on organic substances sub-group additional (6-OrgSub-Add). Are there stations with monitoring results available ? If positive, is there a reason for not reporting via the SoE data flow ?

#. From the dataset provided the country seem to have done a specific monitoring programme in 2010 with big increase of the number of parameters (40 as compared to 8 for other years). Is it correct interpretation? What is the reason for this?

#. In RBD FIVHA5 the reporting has been stopped in 2009. What is the reason for this?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?   

#. As part of the QA process, it is the intention to install new rules for rejecting/flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?

#. Could you check your national databases for potential outliers (see `LINK3 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_ tables 4.4.1.1;) and, if there are outliers, correct them?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more heavy metals and organic substances monitored in lake water in Finland can they be delivered? (6 in sub group heavy metal priority substances and 6 in sub-group heavy metals additionnal where 8 for each are possible, 16 in organic substances sub group priority substances, 8 in sub-group additional with EQS, 9 in sub-group commonly monitored and none in sub-group additional where 78, 11, 21 and 40 respectively in total have been reported by the sum of countries)?

#. Can data for missing years since 2009, esp. in RBDs FIVHA5 and FIVHA4, be redelivered? 

#. Can more years with measuring "preferred" hazardous substances be reported to increase substances and temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
