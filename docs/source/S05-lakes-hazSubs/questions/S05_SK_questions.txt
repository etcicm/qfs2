
**Clarifying questions on data in current database**

#. Not much data have been reported on heavy metals (4 stations for 20 lakes). Are there more stations where these substances are measured?  Or is the low reporting due to very low concentrations or other reasons?

#. No data have been reported on organic substances in year 2007 and 2009. Are there more stations where these substances are measured in these years?  Or is the absence of reporting due to very low concentrations or other reasons?

#. From the dataset provided the country seem to have changed the monitoring programme with very variable number of stations between years. What is the reason for this change?

#. In all lakes reported the reporting stopped in 2009 and was low in 2010 in many lakes. What is the reason for this?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?   

#. As part of the QA process, it is the intention to install new rules for rejecting/flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?

#. Could you check your national databases for potential outliers (see `LINK3 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_ tables 4.4.1.1;) and, if there are outliers, correct them?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more organic substances monitored in lake water in Slovakia and can they be delivered? (35 in organic substances sub group priority substances, 11 in sub-group additional with EQS, 12 in sub-group commonly monitored and 8 in sub-group additionnal where 78, 11, 21 and 40 respectively in total have been reported by the sum of countries)?

#. Can data for missing years 2009 and before 2008 be redelivered? 

#. Can more years with measuring "preferred" hazardous substances be reported to increase substances and temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
