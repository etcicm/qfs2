
**Clarifying questions on data in current database**

#. No data have been reported on the two heavy metal sub-groups (1-HeaMet-PS and 2-HeaMet-Add) and organic substances sub group priority substances (3-OrgSub-PS) in years 2008 and 2009. Are there more stations where these substances are measured in these years?  Or is the stop in reporting due to very low concentrations or other reasons?

#. In 2012, data have been reported only for RBDs GBNIIENW. In 2011 data have been reported only for RDBs IEGBNISH, IESW and IEWE. What is the reason for this?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?   

#. As part of the QA process, it is the intention to install new rules for rejecting/flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?

#. Could you check your national databases for potential outliers (see `LINK3 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_ tables 4.4.1.1;) and, if there are outliers, correct them?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more heavy metals and organic substances monitored in lake water in Ireland and can they be delivered? (4 in sub group heavy metal priority substances and 4 in sub-group heavy metals additional where 8 for each are possible, 19 in organic substances sub group priority substances, 3 in sub-group additional with EQS, 4 in sub-group commonly monitored and 1 in sub-group additional where 78, 11, 21 and 40 respectively in total have been reported by the sum of countries)?

#. LOQ values have not been reported in 2008 and 2009. Is it considered realistic to supply with LOQ values for these years - in particular for substances subject to review for list of priority substances?

#. Can data for missing years 2008 and 2009, be redelivered? 

#. Can more years with measuring "preferred" hazardous substances be reported to increase temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
