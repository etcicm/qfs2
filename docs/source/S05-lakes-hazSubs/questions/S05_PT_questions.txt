
**Clarifying questions on data in current database**

#. Not much data have been reported on organic substances other sub-groups than priority substances. Are there more stations where these sub-groups are measured (in particular pesticides) in these years?  Or is the low reporting due to very low concentrations or other reasons?

#. Organic substances additional sub-group has not been reported. Are there stations with monitoring results available ? If positive, is there a reason for not reporting via the SoE data flow ?

#. From the dataset provided the country seem to have changed the monitoring programme in 2011 with doubling of the number of parameters Is this correct interpretation?. What is the reason for this change?

#. In RBDs PTRH3 and PTRH8 the reporting on organic substances stopped in 2010. What is the reason for this?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?   

#. As part of the QA process, it is the intention to install new rules for rejecting/flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?

#. Could you check your national databases for potential outliers (see `LINK3 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_ tables 4.4.1.1;) and, if there are outliers, correct them?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more heavy metals and organic substances monitored in lake water in Portugal and can they be delivered? (7 in sub group heavy metal priority substances and 7 in sub-group heavy metals additional where 8 for each are possible, 20 in organic substances sub group priority substances, 1 in sub-group additional with EQS, 4 in sub-group commonly monitored and none in sub-group additional where 78, 11, 21 and 40 respectively in total have been reported by the sum of countries)?

#. LOQ values have been reported since 2008 but not for all values in 2012. Is it considered realistic to supply with LOQ values for these data - in particular for substances subject to review for list of priority substances?

#. Can data for missing years since 2008, especially in 2010 for heavy metal and 2012 for organic substances and esp. in RBDs PTRH3 and PTRH8, be redelivered? 

#. Can more years with measuring "preferred" hazardous substances be reported to increase substances and temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
