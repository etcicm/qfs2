﻿
About table 2.3 and 2.4
-------------

.. start-text1-placeholder

Hazardous substances data by Germany have been reported for the period from 2007 on. However, only additional heavy metals for one station have been reported consistently. Additionally, some priority heavy metal measurements exist for 2008 and 2010-2011, priority organic substances for 2009-2011 and additional organic substances for 2010-2011. There are no dissolved fractions of heavy metals in the database. There are only little data available for the most recent year (2012).

Either LOD or LOQ values have been reported for the majority of records in 2009, 2010 and 2011, but not in other reported years.

.. end-text1-placeholder


About table 2.5
-------------

.. start-text2-placeholder

Germany has reported data from 12 lake stations in three different RBDs. There are only few stations with hazardous substances monitoring available for the period, and only one for recent years.

.. end-text2-placeholder



