﻿
About table 2.3 and 2.4
-------------

.. start-text1-placeholder

Slovenia has reported hazardous substances data for the period from 2009 on. The reporting has been consistent throughout the period, with both heavy metals (priority and additional) and priority organic substances reported, with some additional organic substances in 2011 and 2012. The number of reported determinands has been increasing throughout the period. Dissolved fractions of heavy metals have been reported from 2011 on.

Either LOD or LOQ values have been reported for the majority of records.

.. end-text1-placeholder


About table 2.5
-------------

.. start-text2-placeholder

Slovenia has reported hazardous substances data from seven lake stations in both two RBDs in total, however all but one station are located in the RBD SI_RBD_1.

.. end-text2-placeholder



