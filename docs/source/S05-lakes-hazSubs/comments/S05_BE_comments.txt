﻿
About table 2.3 and 2.4
-------------

.. start-text1-placeholder

Belgium has reported data on all groups of hazardous substances for the period from 2009 on, and reporting was consistent throughout the period. However, there was only one lake station reported with monitored organic substances, and up to four for heavy metals. Dissolved fractions of heavy metals have been reported for 2010 and subsequent years.

Either LOD or LOQ values have been reported consistently for the majority of records.

.. end-text1-placeholder


About table 2.5
-------------

.. start-text2-placeholder

In total, four distinct stations have been reported in Belgium, in two RBD altogether. The number of distinct stations was generally increasing, with the majority being reported in the first year of the reporting period.

.. end-text2-placeholder



