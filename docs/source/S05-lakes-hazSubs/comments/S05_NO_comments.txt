﻿
About table 2.3 and 2.4
-------------

.. start-text1-placeholder

Norway has reported hazardous substances data for heavy metals only, with gaps throughout the period. The data were delivered for the periods up to 1995; 2000; 2002; 2004-2006; and from 2009 on. Both priority heavy metals as well as some additional have been reported. In the dataset, there are no data on dissolved fractions of heavy metals and neither organic hazardous substances.

The majority of reported records does not have corresponding LOD or LOQ values.

.. end-text1-placeholder


About table 2.5
-------------

.. start-text2-placeholder

Norway has reported hazardous substances data for 123 lake stations in ten distinct RBDs in total. The most stations with hazardous substances monitoring have been reported in the period 2004-2006, with a generally decreasing trend in station quantity, with only three stations reported for 2012. The most data were reported for the RBD NO1105.

.. end-text2-placeholder



