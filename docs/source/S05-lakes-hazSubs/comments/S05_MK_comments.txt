
About table 2.3 and 2.4
-------------

.. start-text1-placeholder

Macedonia has only reported hazardous substances data for 2010. This included heavy metals and priority organic substances. No dissolved fractions of heavy metals have been reported. There are no corresponding LOD or LOQ values reported for any record.

.. end-text1-placeholder


About table 2.5
-------------

.. start-text2-placeholder

A total of five hazardous substances stations have been reported, with no corresponding RBDs, since there are no defined in Macedonia.

.. end-text2-placeholder



