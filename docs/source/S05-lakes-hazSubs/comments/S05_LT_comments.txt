﻿
About table 2.3 and 2.4
-------------

.. start-text1-placeholder

Lithuania has reported hazardous substances data for the period 1993-2004 and again in 2008 for a single year. Priority and additional heavy metals have been reported throughout the reporting years, while there were no organic substances data reported for 1997, 2000 and 2004. Likewise, there have been no dissolved fractions of heavy metals reported for any year.

Either LOD or LOQ values have been reported for some records. The share of records with corresponding LOD/LOQ varies throughout the period.

.. end-text1-placeholder


About table 2.5
-------------

.. start-text2-placeholder

There are 22 stations with reported hazardous substances data. The number of these varies for each year throughout the period. The data were reported without corresponding RBDs, except for 2008 data where all belong to RBD LT1100.

.. end-text2-placeholder



