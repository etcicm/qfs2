﻿
About table 2.3 and 2.4
-------------

.. start-text1-placeholder

Greece has only reported data for 2002, 2003 and 2004, but there were no data on stations reported - only station codes without attributes. Heavy metals have been reported for 2002 and 2003, and one priority organic hazardous substance for 2004. No dissolved fractions of heavy metals have been reported.

There are only a few LOD or LOQ values available, with majority of records without these.

.. end-text1-placeholder


About table 2.5
-------------

.. start-text2-placeholder

For stations which monitored hazardous substances, there are no attributes reported, including RBDs. Only codes have been provided by the authorities, and further cooperation would be needed to retreive basic station attributes.

.. end-text2-placeholder



