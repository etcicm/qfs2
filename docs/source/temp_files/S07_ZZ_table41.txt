﻿.. exceltable:: Table 4.1: ﻿Number of groundwater stations with records reported, by determinand and year
	:file: ../../S07-GW-nutrients/tables/S07_ZZ_table41.xls
	:header: 1
	:sheet: 0