
About table 2.3 and 2.4
-----------------------

.. start-text1-placeholder

BA has reported hazardous substances data from year 2002 to 2012. Data on Heavy metals (PS + add) was reported in the whole period,
except of year 2005. HM data is covering almost all determinands and maximum of 20 (out of 35 stations). Dissolved fractions of heavy metals 
are not available. Data on Organic substances (PS, EQS, additional) was reported from 2007 to 2012, covering 23 out of 78 total determinands 
in these groups and maximum of 22 stations.

In the dataset there are no data for determinands from group "Organic substances reported additionally - not in PS list, not with EQS".

LOD or LOQ values have been reported from 2006 onward, around 17 % of records having reported either value.

.. end-text1-placeholder


About table 2.5
---------------

.. start-text2-placeholder

BA reported data in total from 35 distinct river stations. There are no RBDs defined since it is not an EU member state.

During the years there has been a slight increase in the number of annually reported river stations from around 8 in the first year up to 
25 in the year 2010.

.. end-text2-placeholder