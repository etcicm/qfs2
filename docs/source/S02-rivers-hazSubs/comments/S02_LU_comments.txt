
About table 2.3 and 2.4
-----------------------

.. start-text1-placeholder

LU has reported hazardous substances data from year 2004 to 2011. 

Data on Heavy metals (PS + add) was reported only in 2011, covering 3/4 determinands and all 3 stations. 
There were no dissolved fractions of heavy metals reported. 

Data on Organic substances was reported from the beginning, but with gaps in reporting in years 2006 and 2007. 
Data is covering all Organic substances groups and  around half of determinands and all of the stations.

LOD or LOQ values have been reported from 2005 onward, around 83 % of records having reported either value.

.. end-text1-placeholder


About table 2.5
---------------

.. start-text2-placeholder

LU reported data in total from 3 distinct river stations from 1 RBD.

During the years the number of annually reported river stations was constant 3.

.. end-text2-placeholder