
About table 2.3 and 2.4
-----------------------

.. start-text1-placeholder

SK has reported hazardous substances data from year 2003 to 2012.

Data on Heavy metals was reported from the beginning and is covering all of determinands and almost all the stations, 
dissolved fractions have been reported since 2009.

Data on Organic substances was reported from the beginning and is covering around half of all determinands and almost all of the stations.

LOD or LOQ values have been reported from 2006 onward, around 90 % of records having reported either value.

.. end-text1-placeholder


About table 2.5
---------------

.. start-text2-placeholder

SK reported data in total from 70 distinct river stations in 2 RBDs in the whole period. 

During the years the number of annually reported river stations was varying slightly from 18 in the first year to 44 in the last.

.. end-text2-placeholder