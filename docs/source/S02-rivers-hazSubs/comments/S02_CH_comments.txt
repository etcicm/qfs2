
About table 2.3 and 2.4
-----------------------

.. start-text1-placeholder

CH has reported hazardous substances data from year 1999 to 2012. Data on Heavy metals (PS + add) was reported consistently in the whole period 
covering only part of determinands (namely: Cadmium, Lead + Chromium, Copper, Zinc) and maximum of 5 (out of 10 stations). Dissolved fractions 
of heavy metals have been added since 2011. Data on all Organic substances was reported in 2012, covering around a quarter of all determinands 
in these groups and 6 / 10 stations.

LOD or LOQ values have been reported for the whole period, around 80 % records having reported either value.

.. end-text1-placeholder


About table 2.5
---------------

.. start-text2-placeholder

CH reported data in total from 10 distinct river stations in 2 RBDs in this period. 

During the years the number of annually reported river stations was fairly constant with a major increase in 2012 reporting.

.. end-text2-placeholder