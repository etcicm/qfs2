
**Clarifying questions on data in current database**

#. From the dataset provided the country seem to have changed the monitoring programme with drastic increase of the number of stations and parameters in 2003 and again in 2010 but also some stations disappearing in the 3 last years. What is the reason for this change?

#. In RBDs UK03 to UK12 the reporting started in 2003 and in UKGBNI and GBNI it started in 2009. What is the reason for this?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?

#. As part of the QA process, it is the intention to install new rules for rejecting/flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?

#. During preparation of improved QA process a series of box plots have been prepared for individual substances  (`LINK2 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/soe_lakes_2014_qa_rules_simple_outliers_v3>`_). In particular for UK, several extreme distributions have been observed. Based on these box plots: are there unit errors - or are the extreme values correct ?

#. Could you check your national databases for potential outliers (see `LINK3 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_ tables 4.3.1.1, 2.3.4 and 2.3.5) and, if there are outliers, correct them?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more heavy metals and organic substances monitored in water in United Kingdom and can they be delivered? (46 in organic substances sub group priority substances, 14 in sub-group commonly monitored and 10 in sub-group additionnal where 78, 11, 21 and 40 respectively in total have been reported by the sum of countries)?

#. LOQ values have been reported since 2009. Is it considered realistic to supply with LOQ values for older data - in particular for substances subject to review for list of priority substances?

#. Can data for missing years from 1992 to 2002, esp. in RBD UK03 to 12 and UKGBNI and GBNI, be redelivered? 

#. Can in particular more years with measuring "preferred" hazardous substances be reported to increase temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
