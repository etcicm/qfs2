
**Clarifying questions on data in current database**

#. Not much data have been reported on organic substances commonly monitored-group in year 2008. Are there more stations where organic substances commonly monitored-group is measured in these years?  Or is the low reporting due to very low concentrations or other reasons?

#. Organic substances additionnal group has not been reported. Are there stations with monitoring results available ? If positive, is there a reason for not reporting via the SoE data flow?

#. From the dataset provided the country seem to have changed the monitoring programme with change of the number of parameters over years. Is the reason for this change due to very low concentrations or other reasons??

#  In most RBDs the reporting covers only 2 years: 2002 and 2008. What is the reason for this?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?   

#. No data have been reported on HS supporting parameters. Are such data available (especially pH, hardness, DOC) corresponding the individual samples for heavy metals?

#. As part of the QA process, it is the intention to install new rules for rejecting/flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?

#. Could you check your national databases for potential outliers (see `LINK3 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_ tables 4.3.1.1, 2.3.4 and 2.3.5) and, if there are outliers, correct them?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more heavy metals and organic substances monitored in water in Greece and can they be delivered? (4 in sub group heavy metal priority substances and 4 in sub-group heavy metals additionnal where 8 for each are possible, 12 in organic substances sub group priority substances, 8 in sub-group additionnal with EQS, 5 in sub-group commonly monitored and none in sub-group additionnal where 78, 11, 21 and 40 respectively are possible)

#. Can Greece reconsider delivering data in WISE SoE?

#. LOQ values have been reported in 2008. Is it considered realistic to supply with LOQ values for older data - in particular for substances subject to review for list of priority substances?

#. Can data for missing years 2005-2007 and 2009-2012 be redelivered? 

#. Can more years with measuring "preferred" hazardous substances be reported to increase temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
