
**Clarifying questions on data in current database**

#. Not much data have been reported on additional organic substances group only reported in year2012. Are there more stations where additional organic substances group is measured in these years?  Or is low reporting due to very low concentrations or other reasons?

#. From the dataset provided the country seem to have increased the number of parameters monitored in 2012. What is the reason for this change?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?   

#. As part of the QA process, it is the intention to install new rules for rejecting/flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?

#. During preparation of improved QA process a series of box plots have been prepared for individual substances  (`LINK2 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/soe_lakes_2014_qa_rules_simple_outliers_v3>`_). In particular RS seem to have outliers. Based on these box plots: are there unit errors - or are the extreme values correct ?

#. Could you check your national databases for potential outliers (see `LINK3 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_ tables 4.3.1.1, 2.3.4 and 2.3.5) and, if there are outliers, correct them?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more organic substances monitored in water in Republic of Serbia and can they be delivered? (24 in organic substances sub group priority substances, 8 in sub-group additional with EQS, 6 in sub-group commonly monitored and 8 in sub-group additionnal where 78, 11, 21 and 40 respectively in total have been reported by the sum of countries)?

#. LOQ values have been reported in 2009 and 2012. Is it considered realistic to supply with LOQ values for 2010 and 2011 and for older data - in particular for substances subject to review for list of priority substances?

#. Can data for missing years: 2008 for commonly monitored organic substances and before 2012 for additional organic substances, be redelivered? 

#. Can more years with measuring "preferred" hazardous substances be reported to increase temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
