
**Clarifying questions on data in current database**

#. Not much data have been reported on additional, commonly  monitored organic substances-group at all (only one substance and one station since year 2001). Are there more stations where Organic substances/Additional commonly monitored-group is measured in these years?  Or is the stop in reporting due to very low concentrations or other reasons?

#. Not much data have been reported on Organic substances/additional substances with EQS-group since year 2003, the available timeserie is short (3 years). Are there more stations where Organic substances/additional substances with EQS-group is measured in these years?  Or is the stop in reporting due to very low concentrations or other reasons?

#. No substance from the additional-group of Organic substances has been reported. Are there stations with monitoring results available ? If positive, is there a reason for not reporting via the SoE data flow ?

#. From the dataset provided the country seem to have changed the monitoring programme with drastic reduction of the number of stations and parameters (up to 282 stations in 2004 and 28 parameters in 2003, 6 stations and 12 parameters in 2012) and a light watching for 3 years followed by one year with more monitoring. What is the reason for this change?

#. In RBDs AT2000 and AT5000 the reporting stopped (or nearly stopped) in 2007. What is the reason for this?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ?  Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?

#. No data have been reported on HS supporting parameters. Are such data available (especially pH, hardness, DOC) corresponding the individual samples for heavy metals?

#. As part of the QA process, it is the intention to install `new rules for rejecting/flagging outliers. <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_
   Is there experience from similar QA procedures on your national datasets?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more organic substances monitored in water in Austria (30 are reported and 150 are possible)?

#. LOQ values have been reported since 2006. Is it considered realistic to supply with LOQ values (especially for periods 2000-2005 and 2011-2012) in particular for substances subject to review for list of priority substances?

#. Can (more) data on organic substances-groups (priority, additional with EQS, commonly monitored and additionnal) be (re)delivered?

#. Can more years with measuring "preferred" hazardous substances be reported to increase temporal coverage? 

#. Can data for missing years since 2000, esp. in RBD AT2000 and AT 5000, be redelivered? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators




