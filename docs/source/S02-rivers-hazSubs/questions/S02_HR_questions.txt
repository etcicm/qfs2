
**Clarifying questions on data in current database**

#. Not much data have been reported on organic substances commonly monitored and additionnal-groups in years 2009-201. Are there more stations where these 2 group are measured in these years?  Or is the low reporting due to very low concentrations or other reasons?

#. From the dataset provided the country seem to have changed the monitoring programme with increase in monitoring for some stations and stop for other stations and stop for some parameters (arsenic, chloroalcanes 10-13...) in recent years. What is the reason for this change?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?   

#. As part of the QA process, it is the intention to install new rules for rejecting/flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?

#. Could you check your national databases for potential outliers (see `LINK3 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_ tables 4.3.1.1, 2.3.4 and 2.3.5) and, if there are outliers, correct them?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more heavy metals and organic substances monitored in water in Croatia and can they be delivered (a maximum of 7 in sub-group heavy metals additionnal where 8 are possible, 25 in organic substances sub group priority substances, 10 in sub-group additionnal with EQS, 1 in sub-group commonly monitored and 4 in sub-group additionnal where 78, 11, 21 and 40 respectively in total have been reported by the sum of countries)?

#. LOQ values have been reported since 2006 except 2008 and 2009. Is it considered realistic to supply with LOQ values for 2008 and 2009 and for older data - in particular for substances subject to review for list of priority substances?

#. Can more years with measuring "preferred" hazardous substances ie before 2003 be reported to increase temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
