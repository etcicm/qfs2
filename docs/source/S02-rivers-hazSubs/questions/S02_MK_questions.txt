
**Clarifying questions on data in current database**

#. All groups of organic substances have not been reported. Are there stations with monitoring results available ? If positive, is there a reason for not reporting via the SoE data flow ?

#. Reported monitoring results are understood as the total sample fraction unless reported for heavy metals (dissolved). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?   

#. As part of the QA process, it is the intention to install new rules for rejecting/flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?

#. During preparation of improved QA process a series of box plots have been prepared for individual substances  (`LINK2 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/soe_lakes_2014_qa_rules_simple_outliers_v3>`_). In particular for GB and IT, several extreme distributions have been observed but for a few substances, also MK, PL, RS, SE and XK seem to have outliers. Based on these box plots: are there unit errors - or are the extreme values correct ?

#. Could you check your national databases for potential outliers (see `LINK3 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_ tables 4.3.1.1, 2.3.4 and 2.3.5) and, if there are outliers, correct them?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more heavy metals and organic substances monitored in water in MK and can they be delivered? (3 in sub group heavy metal priority substances and 3 in sub-group heavy metals additional where 8 for each are possible, none in organic substances sub group priority substances, sub-group additional with EQS, sub-group commonly monitored and sub-group additionnal where 78, 11, 21 and 40 respectively in total have been reported by the sum of countries)?

#. LOQ values have been reported since 2011. Is it considered realistic to supply with LOQ values for older data - in particular for substances subject to review for list of priority substances?

#. Can data for missing year 2009 be redelivered? 

#. Can more years with measuring "preferred" hazardous substances, in particular organic substances be reported to increase temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
