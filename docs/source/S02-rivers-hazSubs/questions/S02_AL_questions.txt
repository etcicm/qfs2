
**Clarifying questions on data in current database**

#. Not much data have been reported on heavy metals hazardous-group in year 2010. Are there more stations where heavy metals hazardous-group is measured in 2010 or more recently?  Or is the stop in reporting due to very low concentrations or other reasons?

#. heavy metals additionnal (sub-group), organic substances and pesticides-groups have not been reported. Are there stations with monitoring results available ? If positive, is there a reason for not reporting via the SoE data flow ?

#. Reported monitoring results are understood as the total sample fraction (not dissolved for heavy metals). Is this correct interpretation ? Are heavy metals reported as dissolved or total or both?

#. In case that the analytical method for any substance will include a filtration step in the laboratory sample preparation: will this be known in the national data base ?   

#. No data have been reported on HS supporting parameters. Are such data available (especially pH, hardness, DOC) corresponding the individual samples for heavy metals?

#. As part of the QA process, it is the intention to install `new rules for rejecting/flagging outliers. <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_
   Is there experience from similar QA procedures on your national datasets?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are there more heavy metals monitored in water in Albania (2 are reported in group heavy metals priority substances and 2 in group additional heavy metals and 8 are possible in both groups)?

#. LOQ values have been reported in 2010 for 2 values. Does LoQ and/or LoD exist for the other 6 values and can they be delivered?

#. Can (more) data on heavy metals additionnal sub-group, but also on organic substances and pesticides-groups be delivered?

#. Can more years with measuring "preferred" hazardous substances be reported to increase temporal coverage? 

#. Are other data series for "preferred" hazardous substances available, e.g. from WFD monitoring stations, and can they be delivered?

#. No RBD was reported for Albania, are there RBDs defined in Albania corresponding to the WFD RBDs or not?


**Links/references**

Please provide further references to

#. National water hazardous substance reports

#. National water hazardous substance indicators
