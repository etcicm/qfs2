
**Clarifying questions on data in current database**

#. Can you provide data on hazardous substaves before 2009, if available
#. Can you provide at least rough coordinates of groundwater monitoring stations (e.g. rounded centroids of municipalities in which the groundwater monitoring stations are located).
#. As part of the QA process, it is the intention to install new rules for testing and flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?
#. Could you check your national databases for potential outliers and, if there are outliers, confirm, correct or delete such records?


**Improving coverage of determinands, temporal and spatial coverage**

#. Can you provide official delineation of river basin districts in Croatia in spatial data format (shapefile, geodatabase)?
#. Are there other groundwater quality monitoring stations available in Croatia? If yes, can you add data on hazardous substances monitored in these stations?


**Links/references**

Please provide further references to

#. National water hazardous substance reports
#. National water hazardous substance indicators




