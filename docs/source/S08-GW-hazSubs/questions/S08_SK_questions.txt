
**Clarifying questions on data in current database**

#. Can you add data on Heavy metals before 2002 and data on the substances from the group "Other organic substances/additional" before 2007, if available?
#. Can you check, whether all data on concentrations of hazardous substances in 2005 and 2006 were really above LOQ/LOD? Such situation is very suspicious.
#. Can you check, whether records reported from 1992 to 2001 as concentrations below LOD were really below LOD and not below LOQ?
#. As part of the QA process, it is the intention to install new rules for testing and flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?
#. Could you check your national databases for potential outliers and, if there are outliers, confirm, correct or delete such records?


**Improving coverage of determinands, temporal and spatial coverage**

#. Can you add data on pesticides monitored in RBD SK30000 Vistula in 2011, if available?


**Links/references**

Please provide further references to

#. National water hazardous substance reports
#. National water hazardous substance indicators




