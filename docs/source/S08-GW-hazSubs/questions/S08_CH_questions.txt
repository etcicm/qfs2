
**Clarifying questions on data in current database**

#. Can you add the data on missing heavy metals (if available) to complete the time series for the last decade (mercury for 2007 - 2012, nickel 2006 - 2008)?
#. Can you provide additional data on hazardous substances for 2000 - 2001? 
#. Are there any data before 2000 available?
#. As part of the QA process, it is the intention to install new rules for testing and flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?
#. Could you check your national databases for potential outliers and, if there are outliers, confirm, correct or delete such records?


**Improving coverage of determinands, temporal and spatial coverage**

#. Is the currently used number of groundwater monitoring stations (cca 40) sufficient for representing of groundwater quality status in the Switzerland?
#. Are there other stations used e.g. in the RBD CH50 Rhone? Currently, 2 stations only are reported for monitoring of nutrients in this RBD.



**Links/references**

Please provide further references to

#. National water hazardous substance reports
#. National water hazardous substance indicators




