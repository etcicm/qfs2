
**Clarifying questions on data in current database**

#. Can you either add the data on dissolved fraction of "Heavy metals/Priority substances" for 2006 or data on total (non-dissolved) fraction for the period 2008 - 2011, to get the uninterrupted time series of these monitored determinands?
#. Can you check, whether the data reported as "below LOD" in 2006 - 2011 are really below LOD and not below LOQ?
#. As part of the QA process, it is the intention to install new rules for testing and flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?
#. Could you check your national databases for potential outliers and, if there are outliers, confirm, correct or delete such records?


**Improving coverage of determinands, temporal and spatial coverage**

#. Can you add the data on Heavy metals in 2007 and data on Pesticides and Other organic substances in 2009 - 2011?
#. Can you provide data on hazardous substances before 2006?
#. Are there additional groundwater quality monitoring stations available, especially in SW and SE part of Serbia? 
#. Can you provide official delineation of groundwater bodies and river basin districts in Serbia in spatial data format (shapefile, geodatabase), if available?


**Links/references**

Please provide further references to

#. National water hazardous substance reports
#. National water hazardous substance indicators




