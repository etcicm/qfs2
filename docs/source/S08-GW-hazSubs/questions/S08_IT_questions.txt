
**Clarifying questions on data in current database**

#. Unfortunately, the formal quality of data reported by Italy during last years is very poor. The datasets are large, but provided with significant delay and containing huge amount of formal errors. Processing of these data is very time consuming, large amount of data (thousands of data records) can not be used and communication with coutry is not effective. The groundwater database manager will contact the national reporter in the near future once more, to solve these problems.
#. The groundwater database manager will contact the national reporter to solve especially these issues:
   - adding of coordinates to the stations where the spatial location is missing (784 stations)
   - linking the groundwater monitoring stations reported in the past without any relation to gw body to the real  groundwater bodies instead of currently used temporal link "virtual" groundwater body IT_Italy_whole_country (3497 stations). 
#. Can you provide additional data on hazardous substances for 2011 and before 2010, to get the datasets comparable with 2010 and 2012?
#. As part of the QA process, it is the intention to install new rules for testing and flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?
#. Could you check your national databases for potential outliers and, if there are outliers, confirm, correct or delete such records?


**Improving coverage of determinands, temporal and spatial coverage**

#. Can you provide data on stations located in the Sicily, Calabria and other regions where stations are not available?


**Links/references**

Please provide further references to

#. National water hazardous substance reports
#. National water hazardous substance indicators




