
**Clarifying questions on data in current database**

#. Are there any data on concentrations of hazardous substances in groundwater hazardous substances available before 2000? If yes, can you provide these data?
#. Can you provide the data on cadmium dissolved, nickel dissolved and lead dissolved in 2009, if available, to fill the gap in time serie?
#. As part of the QA process, it is the intention to install new rules for testing and flagging outliers as described in a supporting document (`LINK1 <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_). Is there experience from similar QA procedures on your national datasets?
#. Could you check your national databases for potential outliers and, if there are outliers, confirm, correct or delete such records?


**Improving coverage of determinands, temporal and spatial coverage**

#. Can you provide data on hazardous substances monitored in Northern Ireland before 2009 and in Scotland before 2006?


**Links/references**

Please provide further references to

#. National water hazardous substance reports
#. National water hazardous substance indicators




