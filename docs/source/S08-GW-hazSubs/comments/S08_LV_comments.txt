
About table 4.4 and 4.5
-----------------------

.. start-text1-placeholder

Latvia reported data on hazardous substances in 2008 (Heavy meatals) and 2009 (Heavy metals + Other organic substances) only.
In 2008, data on cadmium, nickel, lead and mercury monitored in 24 stations were provided. In 2009, data on cadmium and lead from 74 stations were added.
Other organic substances were monitored in 11 stations in 2009.

Concentrations below LOQ as well as below LOD were reported in both years.
The number of records reported as below the LOQ / LOD is adequate to the total number of reported records.

.. end-text1-placeholder


About table 4.6
---------------

.. start-text2-placeholder

Stations in which hazardous substances were monitored in 2008 - 2009 are located in all 4 River basin districts existing in Latvia.

.. end-text2-placeholder

