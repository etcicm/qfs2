
About table 4.4 and 4.5
-----------------------

.. start-text1-placeholder

Czech Republic reported data on Heavy metals for the period 2003 - 2011, data on Other organic substances/Priority substances 
and on Pesticides from 2001 onwards and data on Other organic substances/additional substances from 2008 onwards.
The number of stations from which the data are reported is growing with exception in 2012:
2001 - 2006 cca 270 stations, 2007 - 2008 more than 460 stations and 2009 - 2011 more than 610 stations.
In 2012, no data on Heavy metals were reported and data on Other organic substances were provided for lower number of stations.
Only the data on Pesticides were reported for identical number of stations in comparison to the previous years (more than 610 stations).
The number of monitored substances is stable since 2003 (excluding the missing data on Heavy metals in 2012)

Samples below LOQ were reported during entire period for which the data were provided.
The number of records reported as below the LOQ is adequate to the total number of reported records.

.. end-text1-placeholder


About table 4.6
---------------

.. start-text2-placeholder

The spatial distribution of groundwater monitoring stations is covering the entire area of the Czech Republic. 
The number of stations in which hazardous substances data are monitored is stable since 2009 (612 - 614 stations).
The groundwater quality in all 3 River Basin Districts in the Czech Republic is monitored by sufficient number of
stations (exclusion - reduced number of stations in 2012).

.. end-text2-placeholder

