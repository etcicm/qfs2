
About table 4.4 and 4.5
-----------------------

.. start-text1-placeholder

Macedonia reported data on Heavy metals in 2010 and in 2012.
In 2010, cadmium dissolved, lead dissolved and nickel dissolved were monitored in 8 stations. In 2010, data on cadmium and nickel (both total, non-dissolved) 
monitored in 33 stations are available.
For both years, concentrations of next 3 - 4 substances from the group Heavy metals/additional substances were provided.
Data on pesticides and Other organic substances were not reported.

All reported concentrations were above LOQ/LOD.

.. end-text1-placeholder


About table 4.6
---------------

.. start-text2-placeholder

Delination of River Basin Districts in Macedonia is not available, therefore, the table 4.6 can not be filled with appropriate figures.

.. end-text2-placeholder

