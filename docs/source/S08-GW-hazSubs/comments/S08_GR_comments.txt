
About table 4.4 and 4.5
-----------------------

.. start-text1-placeholder

Greece reported concentrations of cadmium, nickel and lead included in the group Heavy metals/Priority substance and 3 substances
included in the group Heavy metals/additional during 3 years 2006 - 2008. Priority hazardous substances were monitored in 115 - 156 stations, 
additional hazardous substances in 290 - 440 stations.
No data on Pesticides or Other organic substances were provided. 

Concentrations of all reported samples was above LOQ/LOD.

.. end-text1-placeholder


About table 4.6
---------------

.. start-text2-placeholder

The number of monitoring stations in which hazarsous substances were reported per RBD is mostly varying from cca 15 to 50 for the period 2006 - 2008.
In few cases only the number of stations per RBD and year is less than 10.
Reported data were monitored in all RBDs in Greece, i.e. entire area of the country is represented.

.. end-text2-placeholder

