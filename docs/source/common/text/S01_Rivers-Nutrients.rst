
Rivers
******

**Reporting obligation**

Data on rivers are collected through the WISE-SoE data collection process (`River quality EWN-1 <http://rod.eionet.europa.eu/obligations/28>`_).
Biological quality elements in rivers are integrated into reporting of water quality starting from 2012 reporting period.

The data requested on rivers includes the physical characteristics of the river monitoring stations,
proxy pressures on the upstream catchment areas, as well as chemical quality data on nutrients and organic matter,
and hazardous substances in rivers.

It also includes the biological data (primarily calculated as national Ecological Quality Ratios EQR),
as well as information on the national classification systems for each biological quality element and waterbody type.
Determinands which should be reported under this data flow are shown in the following figure.

.. image:: /common/images/determinands_rivers.jpg
   :width: 80%

Closely related to the SoE reporting on rivers water quality is the reporting under the WFD (`WFD River Basin Management
Plan including programme of measures <http://rod.eionet.europa.eu/obligations/521>`_). The new `WFD reporting guidance 2016 <http://icm.eionet.europa.eu/schemas/dir200060ec/resources2014>`_ for the 2nd reporting includes concrete references to SoE
reporting. The information reported by EEA countries can only be interpreted reasonably when streamlined with the WFD reporting.
Combining water quality results with information on ecological status and potential, as foreseen in the WFD reporting guidance, are only possible
if the WFD water body is linked with SoE stations which provide long time series on relevant determinants.

Rivers - Nutrients, Organic Matter and General Physico-Chemical Determinands (see part ONE of quality fact sheet)
=================================================================================================================

**The description and analysis of the reported data on Nutrients, Organic Matter and General Physico-Chemical Determinands is available in part 1
of the quality fact sheets.**