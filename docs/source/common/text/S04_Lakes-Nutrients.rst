
Lakes
*****

**Reporting obligation**

Data on lakes are collected through the WISE-SoE data collection process (`Lake quality EWN-2; <http://rod.eionet.europa.eu/obligations/29>`__).
Biological quality elements in lakes are integrated into reporting of water quality starting from 2012 reporting period.

The data requested on lakes includes the physical characteristics of the lake monitoring stations,
proxy pressures on the upstream catchment areas, as well as chemical quality data on nutrients and organic matter,
and hazardous substances in lakes.

It also includes the biological data (primarily calculated as national Ecological Quality Ratios EQR),
as well as information on the national classification systems for each biological quality element and waterbody type.
determinands which should be reported under this data flow are shown in the following figure.


.. image:: /common/images/determinands_lakes.jpg
   :width: 80%

Closely related to the SoE reporting on lakes water quality is the reporting under the WFD (`WFD River Basin Management Plan including
programme of measures <http://rod.eionet.europa.eu/obligations/521>`__ ). The `new WFD reporting guidance 2016 <http://icm.eionet.europa.eu/schemas/dir200060ec/resources2014>`__ for the 2nd reporting includes concrete references to SoE reporting.
The information reported by EEA countries can only be interpreted reasonably when streamlined with the WFD reporting.
Combining water quality results with information on ecological status and potential, as foreseen in the WFD reporting guidance,
are only possible if the WFD water body is linked with SoE stations which provide long time series on relevant determinants.


Lakes - Nutrients, Organic Matter and General Physico-Chemical Determinands (see part ONE of quality fact sheet)
================================================================================================================

**The description and analysis of the reported data on Lake Nutrients, Organic Matter and General Physico-Chemical Determinands is available in part 1
of the quality fact sheets.**
