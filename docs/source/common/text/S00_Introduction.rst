Introduction
************

The European Environment Agency (EEA) manages water data and information reported either voluntarily by EEA member countries (water quality in groundwater, rivers, 
lakes; emissions of pollutants and water quantity); and data reported via REPORTNET under EU water directives: Water Framework Directive (WFD), Bathing Water Directive 
(BWD), Urban Waste Water Treatment Directive (UWWTD), Nitrates Directive (NiD) and Drinking Water Directive (DWD).

Reported data are processed at EEA and stored in water data centre. They can be also accessible on EEA home page. 
Data reported under Nitrates Directive (NiD) and Drinking Water Directive (DWD) are not yet available at EEA water data centre home page.

The data are generally used in a range of products that are described in more detail in the following sections. 
All information will be used to develop a European picture on water quality and water resources in a comparable way and to identify potential problem areas at the 
European level. Assessments are also made periodically on the impact of particular socio-economic sectors on water (e.g. the impact of agriculture on water), 
of particular issues (e.g. Nutrients in European ecosystems), and assessments based on methodologies for weighted quality indices.

The aim of this Data Quality Factsheet is to support clean-up and error corrections in the data member countries now have reported for 15-20 years. 
These errors have resulted from different steps in the data handling process and it often cannot clearly be specified were the error occurred. 
As we are now revising the whole QA procedure for the upcoming data call in 2015, it is time to also verify and correct the existing information in our data bases. 
We hope and need to rely on your support as the final authority on your data.

Another aspect is to improve the spatial and temporal coverage and to ensure that the relevant determinands are reported. 
Regarding the selection of the determinands reported, we are focusing the review and correction now on a set of determinands which have a high 
priority for our analysis and are definitely used in the regular products that are listed in the following chapters. These determinands in the following will always 
be referred to as "high priority determinands". The tables that are linked to the summary of your reported data might for technical reasons also contain other than 
these high "priority determinands". We like to ask you however to focus only on the determinands mentioned in the questions.


*   In some cases member countries will be asked for the possibility of reporting additional stations to increase the spatial coverage or density of stations. Another set of questions might simply ask for the reason, why data have not been reported for some of the RBDs. Remark that data should not be reported during this consultation round, but during the next regular reporting in the autumn 2015.

*   EEA water quality indicators are for trend assessments based on consistent time series with some gap filling. 
    For a single country, consistent time series are established for the defined period (e.g. 1992-2012; or 2000-2012) with some gap filling (e.g. up to 3 years). 
    Besides the restricted gap filling, only stations with values for all years in the defined period are used. This ensures that any trend is based on a real change in the values of observations and not just due to the change in the stations included.

*   In the current data set the reporting of some high priority determinands has stopped or there has been a change in the determinands in the database
    The introduction of dissolved metals is due in particular to the raising at WG E (chemicals) during the prioritisation exercise that the dissolved fraction 
    is the most relevant for aquatic life. There is however an equilibrium 
    between dissolved and total, in which pH hardness and O2 are among the parameters influencing.
    EEA wants to clarify if these changes are real changes or it has been errors/misinterpretations introduced in compiling the databases. 
    In addition, the aim is to ensure that the high priority determinands (e.g. nitrate or orthophosphate) have as complete a coverage as possible.

*   As part of improving the QA process for hazardous substances (HS), the Water Topic Centre ETC/ICM has conducted 
    `a statistical analysis <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/soe_lakes_2014_qa_rules_simple_outliers_v3>`_
    based on disaggregated data in rivers and lakes. This provides diagrams by substance representing the distribution of values with boxplots for all countries providing data.
    It is suspected that several country datasets are likely to contain unit errors. Based on this, 
    `new rules <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/waterbase_lakes_qadocument_extension_v3>`_
    have been developed for the individual HS. Specific questions are included for a few countries, but these supporting documents may also provide a good entry-point for countries review 
    of their HS datasets for potential errors. Both documents are included in the 
    `folder for supporting documents <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/>`_.
