Groundwater - Hazardous Substances
==================================

**Use of data by EEA**
----------------------

The information will be used to formulate indicators that will be used to assess state and trend of the determinand
and monitor progress with European policy objectives.
In addition to the direct use by EEA, the datasets published in Waterbase are also being used by other Commission services,
e.g. for review of list of Priority Substances and by technical-scientific communities as reference data.

**EEA and ETC/ICM products**

*	`Waterbase Groundwater
	<http://www.eea.europa.eu/data-and-maps/data/waterbase-groundwater-10>`__

*	EEA indicators

	*	`Pesticides in Groundwater (WHS1a)
		<http://www.eea.europa.eu/data-and-maps/indicators/pesticides-in-groundwater>`__

*	EEA reports

	*	`EEA Technical Report  no. 8 / 2011. "Hazardous substances in Europes fresh and marine waters - an overview" <http://www.eea.europa.eu/publications/hazardous-substances-in-europes-fresh>`_
		
			
	*	`ETC/ICM  Technical Report no. 1 / 2013. "Hazardous Substances in European waters -
		Analysis of the data on hazardous substances in groundwater, rivers, transitional, coastal and marine waters reported to the
		European Environment Agency from 1998 - 2010" <http://icm.eionet.europa.eu/ETC_Reports/HazardousSubstancesInWater_2013>`_
			
	*	`ETC/ICM Technical Report no. 1 / 2015. "Hazardous Substances in European waters - Analysis of the data on hazardous substances in groundwater, rivers, lakes, transitional, coastal 
		and marine waters reported to the European Environment Agency from 2002 - 2011" - draft report including EEA comments
		<http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_



**Reported data**
-----------------

From the high number of hazardous substances (HS) reported by SoE data flows, the Water Topic Centre ETC/ICM has labelled a number so called "preferred" HS to 
work as a code-list and to prevent the use of many synonym names for same CAS number and database ID-code. This list is available 
`here <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/list-hazardous-substances-reported-soe-water-quality>`_. 
A grouping of the HS has taken place for the purpose of the Quality Fact Sheet exercise to facilitate overviews in summary tables.

The substances have been divided in three main groups: heavy metals (although arsenic is not a metal but metalloid), pesticides, 
other organic compounds. Pesticides have been selected as a group pursuant to the 
`2006/118/EC DIRECTIVE
<http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32006L0118&qid=1418224572171&from=EN>`__
as the only group of hazardous substances with 
established groundwater quality standards in the Annex I of the Directive (groundwater quality standards are used in GW legislation instead of 
EQS (Environmental quality standard - this discrepancy in terms  has been introduced by the t 2006/118/EC Directive). 
Then each group has been divided in two subgroups (priority substance/additional) according to an occurrence in the Priority Substances list under the 
`DIRECTIVE 2013/39/EU
<http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32013L0039&from=EN>`__
(amending the WFD and EQS directive).

The following grouping has been made for water quality in groundwater:

#.   Heavy metal/ Priority substance: Heavy metals included in Priority Substances list under the 
     `DIRECTIVE 2013/39/EU
     <http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32013L0039&from=EN>`__
     (amending the WFD and EQS directives)         

#.   Heavy metal/additional: Heavy metals reported additionally - not in PS list

#.   Pesticides/Priority substance: Pesticides included in Priority Substances list under the 
     `DIRECTIVE 2013/39/EU
     <http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32013L0039&from=EN>`__
     (amending the WFD and EQS directive)

#.   Pesticides/additional: Pesticides not in PS list, mostly included in ETC/ICM list of "preferred" hazardous substances

#.   Other organic substances/Priority substance: Organic substances except pesticides included in 
     Priority Substances list or Watch List under the 
     `DIRECTIVE 2013/39/EU
     <http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32013L0039&from=EN>`__
     (amending the WFD and EQS directive)

#.   Other organic substances/additional: Organic substances except pesticides not in PS list, mostly included in ETC/ICM list of "preferred" hazardous substances

From the background tables with the list of determinands reported, the corresponding group for each determinand can be seen 
`here <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/list-hazardous-substances-reported-soe-water-quality>`_ 
for groundwater.
 
*Note*: In the following section you will find some selected tables with information concerning the reporting in the period from 1992 to 2012 (if years are available). Some of the comments and questions refer to other tables (background tables). 
You will find these background tables as well as an overview on all background tables in your country folder.

**Determinands**

Table 4.4 provides an overview on the reporting of the hazardous substances sorted into various groups.
Furthermore an overview on the reporting of LOQ and LOD values is given in table 4.5.



.. include:: /S08-GW-hazSubs/comments/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder

.. include:: /S08-GW-hazSubs/tables/S08_ZZ_table44.txt

.. include:: /S08-GW-hazSubs/tables/S08_ZZ_table45.txt
	
*Note*: In the current data set the reporting of some preferred determinands has
stopped or there has been change in the determinands in the database e.g. lead changed to dissolved lead.
EEA wants to clarify if these changes are real changes or it has been errors/misinterpretations introduced in compiling the databases.
In addition, the aim is to ensure that the high priority determinands (e.g. priority substances) have as complete coverage as possible.

**Station coverage**

Table 4.6 shows the number of groundwater stations by River Basin Districts which reported on "preferred" hazardous substances in which period.

.. include:: /S08-GW-hazSubs/comments/ZZ.txt
   :start-after: start-text2-placeholder
   :end-before: end-text2-placeholder
   

.. include:: /S08-GW-hazSubs/tables/S08_ZZ_table46.txt
	
*Note*: One aspect of the quality fact sheets is to improve the spatial coverage and ensure that stations are reported for all RBDs.

*Questions regarding the reporting on hazardous substances in groundwater*
--------------------------------------------------------------------------

.. include:: /S08-GW-hazSubs/questions/ZZ.txt
