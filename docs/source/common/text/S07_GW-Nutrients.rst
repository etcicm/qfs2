
Groundwater
***********

**Reporting obligation**

Data on groundwater bodies are collected through the WISE-SoE data collection process (`Groundwater quality EWN-3 <http://rod.eionet.europa.eu/obligations/30>`_).
Reporting under this obligation is used for EEA Core set of indicators (see EEA products).

The data requested on groundwater include the physical characteristics of the groundwater bodies,
proxy pressures on the groundwater area, location and parameters of groundwater monitoring stations including their relation
to groundwater body as well as chemical quality data on nutrients and organic matter, and hazardous substances in groundwater.
Determinands which should be reported under this data flow are shown in the following figure.

.. image:: /common/images/determinands_gw.jpg
   :width: 50%
   
Closely related to the SoE reporting on groundwater water quality is the reporting under the WFD (`WFD River Basin Management Plan including
programme of measures <http://rod.eionet.europa.eu/obligations/521>`_ ).
The `new WFD reporting guidance 2016 <http://icm.eionet.europa.eu/schemas/dir200060ec/resources2014>`_ for the 2nd reporting includes concrete references to SoE reporting.
The information reported by EEA countries can only be interpreted reasonably when streamlined with the WFD reporting.
Combining water quality results with information on chemical status and potential, as foreseen in the WFD reporting guidance,
are only possible if the WFD water body is linked with SoE stations which provide long time series on relevant determinants.





Groundwater - Nutrients, Organic Matter and General Physico-Chemical Determinands (see part ONE of quality fact sheet)
======================================================================================================================

**The description and analysis of the reported data on Groundwater Nutrients, Organic Matter and General Physico-Chemical Determinands is available in part 1
of the quality fact sheets.**
