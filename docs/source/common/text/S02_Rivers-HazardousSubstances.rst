Rivers - Hazardous Substances
=============================

**Use of data by EEA**
----------------------

The information will be used to formulate indicators that will be used to assess state and trend of the determinand
and monitor progress with European policy objectives.
In addition to the direct use by EEA, the datasets published in Waterbase are also being used by other Commission services,
e.g. for review of list of Priority Substances and by technical-scientific communities as reference data.

**EEA and ETC/ICM products**

*	`Waterbase - Rivers <http://www.eea.europa.eu/data-and-maps/data/waterbase-rivers-10>`_

*	EEA indicators

	*	`Hazardous substances in rivers (WHS2) <http://www.eea.europa.eu/data-and-maps/indicators/hazardous-substances-in-rivers>`_

*	EEA reports

	*	`EEA Technical Report  no. 8 / 2011. "Hazardous substances in Europes fresh and marine waters - an overview" <http://www.eea.europa.eu/publications/hazardous-substances-in-europes-fresh>`_
	
		
	*	`ETC/ICM  Technical Report no. 1 / 2013. "Hazardous Substances in European waters -
		Analysis of the data on hazardous substances in groundwater, rivers, transitional, coastal and marine waters reported to the
		European Environment Agency from 1998 - 2010" <http://icm.eionet.europa.eu/ETC_Reports/HazardousSubstancesInWater_2013>`_
		
	*	`ETC/ICM Technical Report no. 1 / 2015. "Hazardous Substances in European waters - Analysis of the data on hazardous substances in groundwater, rivers, lakes, transitional, coastal 
		and marine waters reported to the European Environment Agency from 2002 - 2011" - draft report including EEA comments
		<http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/hs_etc_technical_report_2015_v4draft>`_
	



**Reported data**
-----------------

From the high number of hazardous substances (HS) reported by SoE data flows, the Water Topic Centre ETC/ICM has labelled a number so called "preferred" HS to work 
as a code-list and to prevent the use of many synonym names for same CAS number and database ID-code. `This list is available here. <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/list-hazardous-substances-reported-soe-water-quality>`_
A few countries have monitored and reported organic substances in addition to this. 
A grouping of the HS has taken place for the purpose of the Quality Fact Sheet exercise to facilitate overviews in summary tables. 
The following grouping has been made for water quality in rivers and lakes:

#.   HeaMet-PS: Heavy metals included in Priority Substances list or Watch List under the 
     `DIRECTIVE 2013/39/EU
     <http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32013L0039&from=EN>`__
     (amending the WFD and EQS directives)

#.    HeaMet-Add: Heavy metals reported additionally - not in PS list

#.   OrgSub-PS: Organic substances included in Priority Substances list or Watch List under the 
     `DIRECTIVE 2013/39/EU
     <http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32013L0039&from=EN>`__
     (amending the WFD and EQS directive)

#.   OrgSub-AddEQS: Organic substances reported additionally - not in PS list but with an 
     Environmental Quality Standard according to 
     `DIRECTIVE 2013/39/EU
     <http://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32013L0039&from=EN>`__

#.   OrgSub-ComMon: Organic substances reported additionally - not in PS list, not with EQS but commonly monitored by several countries - mostly included in ETC/ICM list of "preferred" hazardous substances

#.   OrgSub-Add: Organic substances reported additionally - not in PS list, not with EQS, not in list of "preferred" hazardous substances - only reported by a few countries


From the background tables with the list of determinands reported, the corresponding group for each determinand can be seen 
`here <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/quality-fact-sheets/supporting_documents/list-hazardous-substances-reported-soe-water-quality>`_ 
for rivers and lakes.

*Note*: In the following section you will find some selected tables with information concerning the reporting in the period from 1992 to 2012 (if years are available). 
Some of the comments and questions refer to other tables (background tables). 
You will find these background tables as well as an overview on all background tables in your country folder.

**Determinands**

Table 2.3 provides an overview on the reporting of the hazardous substances sorted into various groups.
Furthermore an overview on the reporting of LOQ and LOD values is given in Table 2.4.

.. include:: /S02-rivers-hazSubs/comments/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder

.. include:: /S02-rivers-hazSubs/tables/S02_ZZ_table23.txt

.. include:: /S02-rivers-hazSubs/tables/S02_ZZ_table24.txt
	
*Note:* In the current data set the reporting of some determinands has stopped or there has been change in the
determinands in the database e.g. lead changed to dissolved lead. EEA wants to clarify if these changes are real changes or it has been
errors/misinterpretations introduced in compiling the databases. In addition, the aim is to ensure that the high priority determinands
(e.g. priority substances) have as complete coverage as possible.

**Station coverage**

Table 2.5 shows the number of river stations by River Basin Districts which reported on hazardous substances in which period.


.. include:: /S02-rivers-hazSubs/comments/ZZ.txt
   :start-after: start-text2-placeholder
   :end-before: end-text2-placeholder

.. include:: /S02-rivers-hazSubs/tables/S02_ZZ_table25.txt	
   
*Note:* One aspect of the quality fact sheets is to improve the spatial coverage and ensure that stations are reported for all RBDs.

*Questions regarding the reporting on hazardous substances in rivers*
---------------------------------------------------------------------

.. include:: /S02-rivers-hazSubs/questions/ZZ.txt
